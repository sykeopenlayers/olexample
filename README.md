# OpenLayers example

This project is an example for using OpenLayers. It's originally made by S.Korhonen/SYKE from OpenLayers tutorial (https://openlayers.org/en/latest/doc/tutorials/bundle.html) with some modifications. It has then been further modified by S.Neuvonen/SYKE with the following changes:

Webpack instead of Parcel

OpenLayers intellisense for Visual Studio Code

kehitysympäristö/debuggaus

cache busting

## Usage

`git clone https://neuvonens@bitbucket.org/sykeopenlayers/olexample.git` (ehkä myös `git clone https://dev.azure.com/sykefi/OpenLayers/_git/OLexample`) 

`npm install`

`npm run-script start`
