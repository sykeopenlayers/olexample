import TileArcGISRest from "ol/source/TileArcGISRest";
import Tile from "ol/Tile";
import ImageArcGISRest from "ol/source/ImageArcGISRest";
import ImageLayer from "ol/layer/Image";

var dynamicArcGISRestMapServiceUrl =
  "http://paikkatieto.ymparisto.fi/arcgis/rest/services/sykemaps/GISAineistot/MapServer/";
var jsonDefinitionUrl = 'https://wwwp2.ymparisto.fi/karpalo/Services/getmapdatajsonfull.aspx?AineistoId=';


var getLayerResolutionLimits = function(layerMinScale, layerMaxScale) {
  // TODO: figure out correct value for this
  var conversionConstant = 5000;

  var layerMinResolution = 1 / (layerMaxScale * conversionConstant);
  var layerMaxResolution = 1 / (layerMinScale * conversionConstant);

  return {
    minResolution: layerMinResolution,
    maxResolution: layerMaxResolution
  };
};


export function getLayerJSONDefinition(layerId) {
  var defUrl = jsonDefinitionUrl + layerId;
  return new Promise(function (resolve, reject) {
    try {
      jQuery.ajax({
        url: defUrl,
        dataType: "json",
        timeout: 2000,
        success: function (dataJSON) {               
            resolve(dataJSON);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.error('  [ERROR] Failed to retrieve layer definition from ' + defUrl + ', status ' + xhr.status + ', ' + thrownError);
            resolve(null);
        }
        });      
    } catch (err) {
      console.error('  [ERROR] Failed to retrieve layer definition from ' + defUrl + ': ' + err);
      resolve(null);
    }
  });
}

export function addArcGISDynamicMapServiceLayerAsImageByDefinition(
  map,
  layerDefinition,
  minScale,
  maxScale,
  useTiling
) {
  var resolutionLimits = getLayerResolutionLimits(minScale, maxScale);
  var layer = null;
  var layerData = {
    url: dynamicArcGISRestMapServiceUrl,
    params: { dynamicLayers: "[" + JSON.stringify(layerDefinition) + "]" }
  };

  if (useTiling) {
    const source = new TileArcGISRest(layerData);

    layer = new Tile({
      source: source,
      maxResolution: resolutionLimits.maxResolution,
      minResolution: resolutionLimits.minResolution
    });
  } else {
    // NO TILING:

    const source = new ImageArcGISRest(layerData);

    layer = new ImageLayer({
      source: source,
      maxResolution: resolutionLimits.maxResolution,
      minResolution: resolutionLimits.minResolution
    });
  }

  map.addLayer(layer);
}
