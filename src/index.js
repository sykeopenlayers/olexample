import "ol/ol.css";
// import '@babel/polyfill';
import { Map, View } from "ol";
import { defaults as defaultControls, ScaleLine } from "ol/control";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import { fromLonLat } from "ol/proj";
import Projection from "ol/proj/Projection";
import { register } from "ol/proj/proj4";
import proj4 from "proj4";
import { addArcGISDynamicMapServiceLayerAsImageByDefinition } from "./ArcGisDynamicMapService.js";
import { getLayerJSONDefinition } from "./ArcGisDynamicMapService.js";

// ETRS35fin projection
proj4.defs(
  "EPSG:3067",
  "+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
);
register(proj4);

var projection = new Projection({
  code: "EPSG:3067",
  extent: [44000, 6594000, 740000, 7782000]
});
//var extent = [420000, 30000, 900000, 350000];

// ETRS35fin projection
proj4.defs(
  "EPSG:3067",
  "+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
);
register(proj4);

var projection = new Projection({
  code: "EPSG:3067",
  extent: [44000, 6594000, 740000, 7782000]
});

// the map
const map = new Map({
  controls: defaultControls().extend([new ScaleLine()]),
  target: "map",
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  view: new View({
    projection: "EPSG:3067",
    center: fromLonLat([24.9, 60.2], "EPSG:3067"),
    zoom: 8
  })
});

// Dynamic layer

//manually
// var layerDynamicDefinition = {
//   id: 109,
//   source: {
//     type: "dataLayer",
//     dataSource: {
//       type: "table",
//       workspaceId: "INSPIRE1",
//       dataSourceName: "GEO.SVW_GEO_HALL100KUNTAMERI"
//     }
//   },
//   drawingInfo: {
//     renderer: {
//       type: "simple",
//       symbol: {
//         type: "esriSFS",
//         style: "esriSFSNull",
//         color: [0, 0, 0, 0],
//         outline: {
//           type: "esriSLS",
//           style: "",
//           color: [230, 0, 0, 255],
//           width: 1
//         }
//       }
//     }
//   }
// }

// addArcGISDynamicMapServiceLayerAsImageByDefinition(
//   map,
//   layerDynamicDefinition,
//   1.0 / 4000000,
//   1.0 / 1000,
//   false
// );

// using Karpalo-service for definition
loadDynamicLayer(109);


async function loadDynamicLayer(datasetId) {
  var layerDynamicDefinition;
  var definitionPromise = getLayerJSONDefinition(datasetId);
  var karpaloDefArray = await definitionPromise;
  if(karpaloDefArray && karpaloDefArray.length>0) {
      layerDynamicDefinition = karpaloDefArray[0]['dynamicDefinition'];
      if(layerDynamicDefinition) {
          addArcGISDynamicMapServiceLayerAsImageByDefinition(
              map,
              layerDynamicDefinition,
              1.0 / 4000000,
              1.0 / 1000,
              false
          );
      }
  }
}
